### https://medium.com/@FelipeFaria/running-a-simple-flask-application-inside-a-docker-container-b83bf3e07dd5

# Downloads Alpine Linux OS to run our app
FROM python

WORKDIR /usr/app/
COPY /app /usr/app/

RUN pip install -r requirements.txt
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

CMD python app.py